﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormularioPrincipal
{
    public partial class FrmLogin : Form
    {
        // DECLARACION DE VARIABLES
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);
        
        //FINAL DE DECLARACION DE VARIABLES

        //=============================================|| METODOS ||=======================================
        public FrmLogin()
        {
            InitializeComponent();
        }//--------------------------

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }//--------------------------

        private void FrmLogin_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }//--------------------------

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }//-------------------------
    }//Final de la clase
}//Final de la solucion
