﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormularioPrincipal
{
    public partial class FrmInformacion : Form
    {
        public FrmInformacion(string Message)
        {
            InitializeComponent();
            lblMensaje.Text = Message;
        }//----------------------------

        private void FrmInformacion_Load(object sender, EventArgs e)
        {
        }//---------------------------

        public static void ConfirmacionForm(string menssage)
        {
            FrmInformacion fm = new FrmInformacion(menssage);
            fm.ShowDialog();
        }//--------------------------
        
        private void btnOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }//----------------------------

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }//-----------------------------

       
    }//Final de la clase
}//Final de la Solucion
