﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormularioPrincipal
{
    public partial class FrmFormulario : Form
    {
        public FrmFormulario()
        {
            InitializeComponent();
            tablaCategoria.DataSource = null;
            tablaCategoria.DataSource = iniciar();
        }//--------------------

        private void btnCerrarFormulario_Click(object sender, EventArgs e)
        {
            this.Close();
        }//--------------------

        private List<String> iniciar() {
            List<String> list;

            list = new List<string>();

            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");

            return list;
        }

    }//Final de la clase
}//Final de la solucion
