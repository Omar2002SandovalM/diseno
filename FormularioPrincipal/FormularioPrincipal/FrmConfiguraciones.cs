﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormularioPrincipal
{
    public partial class FrmConfiguraciones : Form
    {
        public FrmConfiguraciones()
        {
            InitializeComponent();
        }//Final del Constructor
        
        private List<String> iniciar()
        {
            List<String> list;

            list = new List<string>();

            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");
            list.Add("Hola");

            return list;
        }//Metodo de Relleno 

        private void FrmConfiguraciones_Load(object sender, EventArgs e)
        {
            tblUsuarios.DataSource = null;
            tblUsuarios.DataSource = iniciar();

            //--------------------------------------

            tblTipoCliente.DataSource = null;
            tblTipoCliente.DataSource = iniciar();
            //---------------------------------------

            tblDepartamentos.DataSource = null;
            tblDepartamentos.DataSource = iniciar();
        }

    }//Final de la clase
}//Final de la solucion
