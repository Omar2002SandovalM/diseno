﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormularioPrincipal
{
    public partial class FrmSucces : Form
    {
        public FrmSucces(string menssage)
        {
            InitializeComponent();
            lblMensaje.Text = menssage;
        }//---------------------------

        private void FrmSucces_Load(object sender, EventArgs e)
        {
        }//-----------------------------

        public static void ConfirmacionForm(string menssage)
        {
            FrmSucces fm = new FrmSucces(menssage);
            fm.ShowDialog();
        }//--------------------------

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Close();
        }//----------------------------

      
    }//Final de la clase
}//Final de la solucion
