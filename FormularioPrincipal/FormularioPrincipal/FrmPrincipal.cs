﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormularioPrincipal
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }//Final del Constructor

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            DialogResult resultado = new DialogResult();
            Form message = new FrmInformacion("¿SEGURO QUE DESEA SALIR DEL SISTEMA?");
            resultado = message.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                Application.Exit();
                //this.Hide();
            }
            else if(resultado == DialogResult.Cancel)
            {
                message.Close();
            }
        }//----

        public void PantallaOk()
        {
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
        }//--
        //METODO AL INICIAR EL FORMULARIO
        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            PantallaOk();
        }//--
        
        public void SeleccionandoBotones(Bunifu.Framework.UI.BunifuFlatButton Sender)
        {
            btnDashboard.Textcolor = Color.White;
            btnProductos.Textcolor = Color.White;
            btnVentas.Textcolor = Color.White;
            btnCompras.Textcolor = Color.White;
            btnProveedores.Textcolor = Color.White;
            btnTrabajadores.Textcolor = Color.White;
            btnClientes.Textcolor = Color.White;
            btnGanacias.Textcolor = Color.White;

            Sender.selected = true;

            if (Sender.selected)
            {
                Sender.Textcolor = Color.FromArgb(98,195,140);
            }

        }//Final del Metodo para Cambiarles el color a los botones

        //METODO PAR EL MOVIMIENTO DE LA FLECHA
        private void SeguirBoton(Bunifu.Framework.UI.BunifuFlatButton sender)
        {
            Flecha.Top = sender.Top;
        }//------

        //METODOS DE TODOS LOS BOTONES
        private void btnGanacias_Click(object sender, EventArgs e)
        {
            SeleccionandoBotones((Bunifu.Framework.UI.BunifuFlatButton) sender);
            SeguirBoton((Bunifu.Framework.UI.BunifuFlatButton)sender);
        }//--

        private void btnProductos_Click(object sender, EventArgs e)
        {
            SeleccionandoBotones((Bunifu.Framework.UI.BunifuFlatButton)sender);
            SeguirBoton((Bunifu.Framework.UI.BunifuFlatButton)sender);
            AbrirFormulariosEnWrapper(new FrmProductos());
        }//--

        private void btnVentas_Click(object sender, EventArgs e)
        {
            SeleccionandoBotones((Bunifu.Framework.UI.BunifuFlatButton)sender);
            SeguirBoton((Bunifu.Framework.UI.BunifuFlatButton)sender);
        }//--

        private void btnCompras_Click(object sender, EventArgs e)
        {
            SeleccionandoBotones((Bunifu.Framework.UI.BunifuFlatButton)sender);
            SeguirBoton((Bunifu.Framework.UI.BunifuFlatButton)sender);
        }//--

        private void btnTrabajadores_Click(object sender, EventArgs e)
        {
            SeleccionandoBotones((Bunifu.Framework.UI.BunifuFlatButton)sender);
            SeguirBoton((Bunifu.Framework.UI.BunifuFlatButton)sender);
        }//--

        private void btnClientes_Click(object sender, EventArgs e)
        {
            SeleccionandoBotones((Bunifu.Framework.UI.BunifuFlatButton)sender);
            SeguirBoton((Bunifu.Framework.UI.BunifuFlatButton)sender);
        }//--

        private void btnProveedores_Click(object sender, EventArgs e)
        {
            SeleccionandoBotones((Bunifu.Framework.UI.BunifuFlatButton)sender);
            SeguirBoton((Bunifu.Framework.UI.BunifuFlatButton)sender);
        }//--

        private void btnDashboard_Click(object sender, EventArgs e)
        {
            SeleccionandoBotones((Bunifu.Framework.UI.BunifuFlatButton)sender);//Al seleccionar un boton
            SeguirBoton((Bunifu.Framework.UI.BunifuFlatButton)sender);//Mover el Picture Box a cada boton
            AbrirFormulariosEnWrapper(new FrmConfiguraciones());
        }//--
        //FINAL DE TODOS LOS METODOS DE LOS BOTONES

        private Form FormActivado = null;

        private void AbrirFormulariosEnWrapper(Form FormHijo)
        {
            if (FormActivado != null)
                FormActivado.Close();
            FormActivado = FormHijo;
            FormHijo.TopLevel = false;
            FormHijo.Dock = DockStyle.Fill;
            Wrapper.Controls.Add(FormHijo);
            Wrapper.Tag = FormHijo;
            FormHijo.BringToFront();
            FormHijo.Show();
            
        }//-----------------------------------------------------
    }//Final de la Clase
}//Final de la solucion
